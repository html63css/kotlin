package com.example.tableview

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val image: ImageView = findViewById(R.id.Image)
        val button: Button = findViewById(R.id.Button)
        button.setOnClickListener {
            when(Random.nextInt(1,6)){
                1 -> image.setImageDrawable(getDrawable(R.drawable.cat1))
                2 -> image.setImageDrawable(getDrawable(R.drawable.cat2))
                3 -> image.setImageDrawable(getDrawable(R.drawable.cat3))
                4 -> image.setImageDrawable(getDrawable(R.drawable.cat4))
                5 -> image.setImageDrawable(getDrawable(R.drawable.cat5))
                6 -> image.setImageDrawable(getDrawable(R.drawable.cat6))
            }
            val color = Color.argb(255,
                Random.nextInt(0,255),
                Random.nextInt(0,255),
                Random.nextInt(0,255))
            button.setBackgroundColor(color)
        }
    }
}